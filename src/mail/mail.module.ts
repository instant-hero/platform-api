import { Module } from '@nestjs/common';
import {MailerModule} from "@nestjs-modules/mailer";
import {HandlebarsAdapter} from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";
import { join } from 'path';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: process.env.SMTP || 'smtp.gmail.com',
        secure: true,
        auth: {
          user: process.env.MAIL_USER || 'erp.codexio@gmail.com',
          pass: process.env.MAIL_PASS || '0erpCodexio!',
        },
      },
      defaults: {
        from: '"No-Reply (Instant Hero)" <' + (process.env.MAIL_USER || 'erp.codexio@gmail.com') + '>',
      },
      template: {
        dir: join(__dirname, 'templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [],
  exports: [],
})
export class MailModule {}
