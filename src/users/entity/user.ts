import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Disaster} from "../../disasters/entity/disaster";
import {Participation} from "../../disasters/entity/participation";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public username: string;

    @Column()
    public password: string;

    @Column()
    public name: string;

    @Column({nullable: true})
    public description: string;

    @Column()
    public available: boolean = false;

    @Column()
    public isAdmin: boolean = false;

    @OneToMany(() => Participation, participation => participation.user)
    public participations: Participation[]
}
