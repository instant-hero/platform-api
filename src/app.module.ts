import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { User } from './users/entity/user';
import { AuthModule } from './auth/auth.module';
import { Connection } from 'typeorm';
import { DisastersModule } from './disasters/disasters.module';
import { DisasterType } from './disasters/entity/disaster-type';
import { Disaster } from './disasters/entity/disaster';
import {DisasterTypeAction} from "./disasters/entity/disaster-type-action";
import {DisasterTypeActionItem} from "./disasters/entity/disaster-type-action-item";
import {Participation} from "./disasters/entity/participation";
import { MailModule } from './mail/mail.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST || 'localhost',
      port: +(process.env.DB_PORT || '3306'),
      username: process.env.DB_USER || 'root',
      password: process.env.DB_PASS || '',
      database: process.env.DB_NAME || 'instanthero',
      entities: [User, Disaster, DisasterType, DisasterTypeAction, Participation, DisasterTypeActionItem],
      synchronize: true,
      charset: 'utf8mb4_unicode_ci',
      extra: {
        charset: 'utf8mb4_unicode_ci',
      },
    }),
    UsersModule,
    AuthModule,
    DisastersModule,
    MailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
