import {DisasterRepository} from '../repository/disaster.repository';
import {Disaster} from '../entity/disaster';
import {DisasterTypeRepository} from '../repository/disaster-type.repository';
import {DisasterRequest} from '../payload/disaster.request';
import {DisasterResponse} from '../payload/disaster.response';
import {AuthResponse} from '../../auth/payload/auth.response';
import {UserService} from '../../users/service/user.service';
import {EssentialsRequest} from '../payload/essentials.request';
import {FinalMessageRequest} from '../payload/final-message.request';
import {MetaInfoResponse} from '../payload/meta-info.response';
import {Inject} from '@nestjs/common';
import {ClientProxy} from '@nestjs/microservices';
import {DisasterTypeAction} from "../entity/disaster-type-action";
import {ActionRepository} from "../repository/action.repository";
import {ItemRepository} from "../repository/item.repository";
import {VolunteerRequest} from "../payload/volunteer.request";
import {User} from "../../users/entity/user";
import {v4 as uuidv4} from 'uuid';
import {UserRequest} from "../../users/payload/user.request";
import {Participation} from "../entity/participation";
import {ParticipationRepository} from "../repository/participation.repository";
import {MailerService} from "@nestjs-modules/mailer";
import {ParticipationResponse} from "../payload/participation.response";

export class DisasterService {
    constructor(
        private readonly userService: UserService,
        private readonly disasterRepository: DisasterRepository,
        private readonly disasterTypeRepository: DisasterTypeRepository,
        private readonly actionRepository: ActionRepository,
        private readonly itemRepository: ItemRepository,
        private readonly participationRepository: ParticipationRepository,
        @Inject('DISASTERS_SERVICE') private client: ClientProxy,
        private mailService: MailerService
    ) {
    }

    async all(resolvedFrom: boolean, resolvedTo: boolean, typeId: number): Promise<DisasterResponse[]> {
        return (await this.disasterRepository.findByResolvedAndType(
                resolvedFrom,
                resolvedTo,
                typeId)
        ).map(DisasterResponse.fromDisaster)
    }

    async volunteers(id): Promise<ParticipationResponse[]> {
        return (await this.participationRepository.findByDisaster(id))
            .map(ParticipationResponse.fromParticipation);
    }

    async single(id: number): Promise<DisasterResponse> {
        return DisasterResponse.fromDisaster(
            await this.disasterRepository.findOne(id),
        );
    }

    async types(): Promise<MetaInfoResponse[]> {
        return (await this.disasterTypeRepository.find()).map(
            MetaInfoResponse.fromEntity,
        );
    }

    async actions(id: number): Promise<MetaInfoResponse[]> {
        return (await this.actionRepository.findByType(id))
            .map(MetaInfoResponse.fromEntity);
    }

    async items(id: number): Promise<MetaInfoResponse[]> {
        return (await this.itemRepository.findByAction(id))
            .map(MetaInfoResponse.fromEntity);
    }

    async create(model: DisasterRequest): Promise<DisasterResponse> {
        const disaster = new Disaster();

        const disasterType = await this.disasterTypeRepository.findOne(
            model.typeId,
        );

        disaster.coordinates = model.coordinates;
        disaster.description = model.description;
        disaster.dateStarted = new Date();
        disaster.type = disasterType;

        await this.disasterRepository.save(disaster);

        const response = DisasterResponse.fromDisaster(disaster);

        this.client.emit('disaster_happened', response);

        return response;
    }

    async volunteer(
        model: VolunteerRequest,
        userModel: AuthResponse,
        disasterId: number,
    ): Promise<DisasterResponse> {
        let disaster = await this.disasterRepository.findOne(disasterId);

        await this.addVolunteer(userModel, model, disaster);

        await this.disasterRepository.save(disaster);

        return DisasterResponse.fromDisaster(disaster);
    }

    async essentials(
        disasterId: number,
        essentialsModel: EssentialsRequest,
    ): Promise<DisasterResponse> {
        const disaster = await this.disasterRepository.findOne(disasterId);

        disaster.essentials = essentialsModel.essentials;

        await this.disasterRepository.save(disaster);

        return DisasterResponse.fromDisaster(disaster);
    }

    async resolve(
        disasterId: number,
        finalMessageModel: FinalMessageRequest,
    ): Promise<DisasterResponse> {
        const disaster = await this.disasterRepository.findOne(disasterId);

        disaster.resolved = true;
        disaster.dateResolved = new Date();
        disaster.finalMessage = finalMessageModel.message;

        await this.disasterRepository.save(disaster);

        return DisasterResponse.fromDisaster(disaster);
    }

    private async addVolunteer(userModel: AuthResponse, request: VolunteerRequest, disaster: Disaster) {
        const user = userModel
            ? await this.userService.findByUsername(userModel.username)
            : await this.createUser(request);



        for (const actionRequest of request.actions) {
            const action = await this.actionRepository.findOne(actionRequest.id);
            if (!actionRequest.items || !actionRequest.items.length) {
                this.participationRepository.save(
                    DisasterService.createBasicParticipation(action, user, disaster)
                );
                continue;
            }

            for (const itemId of actionRequest.items) {
                const participation = DisasterService.createBasicParticipation(action, user, disaster);
                participation.item = await this.itemRepository.findOne(itemId);

                this.participationRepository.save(participation);
            }
        }

        return disaster;
    }

    private async createUser(request: VolunteerRequest): Promise<User> {
        const pass = uuidv4();
        const registerRequest = new UserRequest();
        registerRequest.username = registerRequest.name = request.email;
        registerRequest.password = registerRequest.confirm = pass;
        await this.userService.register(registerRequest);

        this.mailService.sendMail({
                to: request.email,
                subject: 'Успешна регистрация',
                template: './new-account',
                context: {
                    name: registerRequest.name,
                    password: pass
                }
            }
        ).then(() => console.log('Mail sent to ' + registerRequest.name));

        return this.userService.findByUsername(request.email);
    }

    private static createBasicParticipation(action: DisasterTypeAction, user: User, disaster: Disaster) {
        const participation = new Participation();
        participation.action = action;
        participation.user = user;
        participation.disaster = disaster

        return participation;
    }

}
