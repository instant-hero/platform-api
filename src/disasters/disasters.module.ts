import {Logger, Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from '../users/entity/user';
import {UserRepository} from '../users/repository/user.repository';
import {UserService} from '../users/service/user.service';
import {Disaster} from './entity/disaster';
import {DisasterRepository} from './repository/disaster.repository';
import {DisasterType} from './entity/disaster-type';
import {DisasterTypeRepository} from './repository/disaster-type.repository';
import {DisasterService} from './service/disaster.service';
import {DisasterController} from './controller/disaster.controller';
import {BROKER_OPTS, BROKER_TRANSPORT} from '../broker.config';
import {ClientsModule} from '@nestjs/microservices';
import {DisasterTypeAction} from "./entity/disaster-type-action";
import {DisasterTypeActionItem} from "./entity/disaster-type-action-item";
import {Participation} from "./entity/participation";
import {MailModule} from "../mail/mail.module";
import {ActionRepository} from "./repository/action.repository";
import {ItemRepository} from "./repository/item.repository";
import {ParticipationRepository} from "./repository/participation.repository";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Disaster,
            DisasterRepository,
            DisasterTypeActionItem,
            DisasterTypeAction,
            DisasterType,
            Participation,
            DisasterTypeRepository,
            ActionRepository,
            ItemRepository,
            ParticipationRepository,
            User,
            UserRepository,
        ]),
        ClientsModule.register([
            {
                name: 'DISASTERS_SERVICE',
                transport: BROKER_TRANSPORT,
                options: BROKER_OPTS,
            },
        ]),
        MailModule
    ],
    providers: [DisasterService, UserService],
    controllers: [DisasterController],
})
export class DisastersModule {
}
