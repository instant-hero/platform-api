import {Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn,} from 'typeorm';
import {DisasterType} from './disaster-type';
import {User} from '../../users/entity/user';
import {DisasterTypeAction} from "./disaster-type-action";
import {DisasterTypeActionItem} from "./disaster-type-action-item";
import {Disaster} from "./disaster";

@Entity()
export class Participation {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => User, (user) => user.participations, {
    eager: false,
  })
  public user: User;

  @ManyToOne(() => Disaster, (disaster) => disaster.participations, {
    eager: false,
  })
  public disaster: Disaster;

  @ManyToOne(() => DisasterTypeAction, (action) => action.participations, {
    eager: false,
  })
  public action: DisasterTypeAction;

  @ManyToOne(() => DisasterTypeActionItem, (item) => item.participations, {
    eager: false,
  })
  public item: DisasterTypeActionItem;

}
