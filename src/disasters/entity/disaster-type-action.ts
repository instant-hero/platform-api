import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {DisasterType} from "./disaster-type";
import {DisasterTypeActionItem} from "./disaster-type-action-item";
import {Participation} from "./participation";

@Entity()
export class DisasterTypeAction {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public iconUrl: string;

    @ManyToMany(() => DisasterType, type => type.actions, {eager: false})
    @JoinTable()
    public types: DisasterType[];

    @ManyToMany(() => DisasterTypeActionItem, item => item.actions, {eager: false})
    public items: DisasterTypeActionItem[];

    @OneToMany(() => Participation, participation => participation.action)
    public participations: Participation[]
}
