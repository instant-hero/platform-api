import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {DisasterTypeAction} from "./disaster-type-action";
import {Participation} from "./participation";

@Entity()
export class DisasterTypeActionItem {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public iconUrl: string;

    @ManyToMany(() => DisasterTypeAction, action => action.items, {eager: false})
    @JoinTable()
    public actions: DisasterTypeAction[];

    @OneToMany(() => Participation, participation => participation.item)
    public participations: Participation[]
}
