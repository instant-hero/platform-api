import {Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Disaster} from "./disaster";
import {DisasterTypeAction} from "./disaster-type-action";

@Entity()
export class DisasterType {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public iconUrl: string;

    @OneToMany(() => Disaster, disaster => disaster.type, {eager: false})
    public disasters: Disaster[];

    @ManyToMany(() => DisasterTypeAction, action => action.types, {eager: false})
    public actions: DisasterTypeAction[]
}
