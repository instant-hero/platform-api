import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn,} from 'typeorm';
import {DisasterType} from './disaster-type';
import {Participation} from "./participation";

@Entity()
export class Disaster {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public dateStarted: Date;

  @Column()
  public coordinates: string;

  @ManyToOne(() => DisasterType, (type) => type.disasters, {
    eager: true,
  })
  public type: DisasterType;

  @Column({ nullable: false, default: false })
  public resolved: boolean;

  @Column()
  public description: string;

  @Column({ nullable: true })
  public essentials: string;

  @Column({ nullable: true })
  public finalMessage: string;

  @Column({ nullable: true })
  public dateResolved: Date;

  @Column({ nullable: false, default: 0 })
  public outerHelp: number;

  @OneToMany(() => Participation, participation => participation.disaster)
  public participations: Participation[]
}
