import {EntityRepository, Repository} from "typeorm";
import {DisasterTypeAction} from "../entity/disaster-type-action";

@EntityRepository(DisasterTypeAction)
export class ActionRepository extends Repository<DisasterTypeAction> {

    findByType(typeId: number): Promise<DisasterTypeAction[]> {
        return this.createQueryBuilder('a')
            .leftJoin('a.types', 'type')
            .where('type.id = :id', {id: typeId})
            .getMany()
    }

}
