import {EntityRepository, Repository} from "typeorm";
import {Disaster} from "../entity/disaster";

@EntityRepository(Disaster)
export class DisasterRepository extends Repository<Disaster> {

    findByResolvedAndType(resolvedFrom, resolvedTo, typeId): Promise<Disaster[]> {
        let query = this.createQueryBuilder("d")
            .innerJoinAndSelect('d.type', 'type')
            .where('(resolved = :resolvedFrom OR resolved = :resolvedTo)', {
                resolvedFrom,
                resolvedTo
            });

        if (typeId) {
            query = query.andWhere('typeId = :typeId', {typeId})
        }

        query = query.addOrderBy('dateStarted', "ASC")

        return query.getMany()
    }
}
