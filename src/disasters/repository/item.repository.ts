import {EntityRepository, Repository} from "typeorm";
import {DisasterTypeActionItem} from "../entity/disaster-type-action-item";

@EntityRepository(DisasterTypeActionItem)
export class ItemRepository extends Repository<DisasterTypeActionItem> {

    findByAction(actionId: number): Promise<DisasterTypeActionItem[]> {
        return this.createQueryBuilder('i')
            .leftJoin('i.actions', 'action')
            .where('action.id = :id', {id: actionId})
            .getMany()
    }
}
