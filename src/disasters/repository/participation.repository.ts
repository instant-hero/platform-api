import {EntityRepository, Repository} from "typeorm";
import {Participation} from "../entity/participation";

@EntityRepository(Participation)
export class ParticipationRepository extends Repository<Participation> {

    findByDisaster(disasterId: number): Promise<Participation[]> {
        return this.find({
            relations: ['action', 'item', 'user'],
            where: {
                disaster: {
                    id: disasterId
                }
            }
        })
    }
}
