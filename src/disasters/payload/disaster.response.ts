import {Disaster} from '../entity/disaster';
import {MetaInfoResponse} from './meta-info.response';

export class DisasterResponse {
    constructor(
        public id: number = 0,
        public coordinates: string = '',
        public dateStarted: Date = new Date(),
        public type: MetaInfoResponse = null,
        public resolved: boolean = false,
        public description: string = '',
        public essentials: string = '',
        public finalMessage: string = '',
        public dateResolved: Date = new Date(),
        public outerHelp: number = 0,
    ) {
    }

    public static fromDisaster(disaster: Disaster) {
        return new DisasterResponse(
            disaster.id,
            disaster.coordinates,
            disaster.dateStarted,
            MetaInfoResponse.fromEntity(disaster.type),
            disaster.resolved,
            disaster.description,
            disaster.essentials,
            disaster.finalMessage,
            disaster.dateResolved,
            disaster.outerHelp,
        );
    }
}
