import {MetaInfoResponse} from './meta-info.response';
import {UserResponse} from "../../users/payload/user.response";
import {Participation} from "../entity/participation";

export class ParticipationResponse {
    constructor(
        public id: number = 0,
        public action: MetaInfoResponse,
        public item: MetaInfoResponse,
        public user: UserResponse
    ) {
    }

    public static fromParticipation(participation: Participation) {
        return new ParticipationResponse(
            participation.id,
            participation.action ? MetaInfoResponse.fromEntity(participation.action) : null,
            participation.item ? MetaInfoResponse.fromEntity(participation.item) : null,
            UserResponse.fromUser(participation.user)
        )
    }
}
