export class MetaInfoResponse {
    constructor(
        public id: number = 0,
        public name: string = '',
        public iconUrl: string = ''
    ) {

    }

    static fromEntity(entity: {id, name, iconUrl}) {
        return new MetaInfoResponse(entity.id, entity.name, entity.iconUrl);
    }
}
