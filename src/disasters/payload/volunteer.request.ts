import {ActionRequest} from "./action.request";

export class VolunteerRequest {
    public actions: ActionRequest[] = [];

    public email: string;
}
