import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { DisasterService } from '../service/disaster.service';
import { DisasterRequest } from '../payload/disaster.request';
import { DisasterResponse } from '../payload/disaster.response';
import { AuthPrincipal, Public } from '../../auth/decorator/auth.decorator';
import { EssentialsRequest } from '../payload/essentials.request';
import { FinalMessageRequest } from '../payload/final-message.request';
import { MetaInfoResponse } from '../payload/meta-info.response';
import { ClientProxy } from '@nestjs/microservices';
import {VolunteerRequest} from "../payload/volunteer.request";
import {ParticipationResponse} from "../payload/participation.response";

@Controller('/disasters')
export class DisasterController {
  constructor(private readonly disasterService: DisasterService) {}

  @Get()
  @Public()
  async disasters(
    @Query('resolvedFrom') resolvedFrom = true,
    @Query('resolvedTo') resolvedTo = false,
    @Query('typeId') typeId: number
  ): Promise<DisasterResponse[]> {
    return this.disasterService.all(resolvedFrom, resolvedTo, typeId);
  }

  @Get('/types')
  @Public()
  async types(): Promise<MetaInfoResponse[]> {
    return this.disasterService.types();
  }

  @Get('/types/:id/actions')
  @Public()
  async actions(@Param('id') id: number): Promise<MetaInfoResponse[]> {
    return this.disasterService.actions(id);
  }

  @Get('/actions/:id/items')
  @Public()
  async items(@Param('id') id: number): Promise<MetaInfoResponse[]> {
    return this.disasterService.items(id);
  }

  @Get('/:id')
  @Public()
  async disaster(@Param('id') id: number): Promise<DisasterResponse> {
    return this.disasterService.single(id);
  }

  @Get('/:id/volunteers')
  @Public()
  async volunteers(@Param('id') id: number): Promise<ParticipationResponse[]> {
    return this.disasterService.volunteers(id);
  }

  @Post()
  @Public()
  async report(@Body() model: DisasterRequest): Promise<DisasterResponse> {
    return this.disasterService.create(model);
  }

  @Patch('/:id/guest/volunteer')
  @Public()
  async guestVolunteer(
      @Param('id') id: number,
      @Body() model: VolunteerRequest
  ): Promise<DisasterResponse> {
    return this.volunteer(null, id, model);
  }

  @Patch('/:id/volunteer')
  async volunteer(
    @AuthPrincipal() principal,
    @Param('id') id: number,
    @Body() model: VolunteerRequest
  ): Promise<DisasterResponse> {
    return this.disasterService.volunteer(model, principal, id);
  }

  @Patch('/:id/essentials')
  async essentials(
    @Param('id') id: number,
    @Body() model: EssentialsRequest,
  ): Promise<DisasterResponse> {
    return this.disasterService.essentials(id, model);
  }

  @Patch('/:id/resolved')
  async resolve(
    @Param('id') id: number,
    @Body() model: FinalMessageRequest,
  ): Promise<DisasterResponse> {
    return this.disasterService.resolve(id, model);
  }
}
